import cv2
import numpy as np
# debug start ----------------------------------------------------------------------
import os
import shutil
import time
from pprint import pprint
# debug end ----------------------------------------------------------------------

class homographyTransform:
    '''
    Transform color & depth image taken from angle into it looks like taken from directly above
    with focal length, principal point, camera installation angles & image size. \n
    Convert coordinates on image looks like taken from directly above to those of original image.

    Attributes
    ----------
    fx, fy : float
        focal length
    ppx, ppy : float
        principle point
    sin_x, sin_y, sin_z, cos_x, cos_y, cos_z, tan_x, tan_y, tan_z : float
        trigonometric function of each angle
    sin_x_rev, sin_y_rev, sin_z_rev, cos_x_rev, cos_y_rev, cos_z_rev, tan_x_rev, tan_y_rev, tan_z_rev : float
        trigonometric function of each negative angle
    mtx_mv_origin_center, mtx_mv_origin_center_rev : Numpy Array
        matrix to move origin from top-left to center of image & to do opposite 
    mtx_norm, mtx_norm_rev : Numpy Array
        matrix to normalize coordinates & to do opposite 
    mtx_rot_mv, mtx_rot_mv_rev : Numpy Array
        matrix to rotate & move image & to do opposite 
    mtx_magnification, mtx_magnification_rev : Numpy Array
        matrix to magnify normalized image & to do opposite 
    mtx_mv_origin_topleft, mtx_mv_origin_topleft_rev : Numpy Array
        matrix to move origin to top-left of transformed image & to do opposite 
    mtx_trans, mtx_trans_rev : Numpy Array
        matrix to transform image from low angle to looks like taken from directly above & to do opposite 

    Notes
    ----------
    If you want to change focal length, principal point, camera installation angles & image size, you need to create new instance.
    '''
    def __init__(self, focal_length, principle_point, angles_deg, image_size=(720,1280)):
        '''
        constructor
    
        Parameters
        ----------
        focal_length : tuple
            focal length as (fx, fy)
        principle_point : tuple
            principle point as (ppx, ppy)
        angles_deg : tuple
            camera installation angles (degree) as (x, y, z)
        image_size : tuple
            color & depth image size as (width, height)
    
        Returns
        -------
        None
        '''
        self.fx, self.fy = focal_length
        self.ppx, self.ppy = principle_point
        self.img_height, self.img_width = image_size
        self._calc_trigonometric_function(angles_deg)
        # calc matrices
        self._create_mtx_mv_origin_center()
        self._create_mtx_norm()
        self._create_mtx_rot_mv()
        self._create_mtx_magnification_and_mtx_mv_origin_topleft()
        self._create_mtx_trans()

    def _calc_trigonometric_function(self, angles_deg):
        '''
        Calculate trigonometric function of each angle & negative angle.
    
        Parameters
        ----------
        angles_deg : tuple
            camera installation angles (degree) as (x, y, z)
    
        Returns
        -------
        None
        '''
        angle_rad = np.deg2rad(angles_deg)

        # from low angle to looks like taken from directly above
        self.sin_x, self.sin_y, self.sin_z = np.sin(angle_rad)
        self.cos_x, self.cos_y, self.cos_z = np.cos(angle_rad)
        self.tan_x, self.tan_y, self.tan_z = np.tan(angle_rad)

        # from looks like taken from directly above to low angle
        self.sin_x_rev, self.sin_y_rev, self.sin_z_rev = self.sin_x*-1, self.sin_y*-1, self.sin_z*-1
        self.cos_x_rev, self.cos_y_rev, self.cos_z_rev = self.cos_x, self.cos_y, self.cos_z
        self.tan_x_rev, self.tan_y_rev, self.tan_z_rev = self.tan_x*-1, self.tan_y*-1, self.tan_z*-1

    def _create_mtx_mv_origin_center(self):
        '''
        Create matrix to move origin from top-left to center of image & to do opposite.
    
        Parameters
        ----------
        None
    
        Returns
        -------
        None
        '''
        # matrix to move origin from top-left to center of image. 
        self.mtx_mv_origin_center = np.array([
            [1,0,-self.ppx],
            [0,1,-self.ppy],
            [0,0,1]
        ])
        # opposite of mtx_mv_origin_center
        self.mtx_mv_origin_center_rev = np.array([
            [1,0,self.ppx],
            [0,1,self.ppy],
            [0,0,1]
        ])

    def _create_mtx_norm(self):
        '''
        Create matrix to normalize coordinates & to do opposite.
    
        Parameters
        ----------
        None
    
        Returns
        -------
        None
        '''
        # matrix to normalize coordinates
        # in other words, matrix to move coordinate points to Z=1 plane of camera coordinate system.
        self.mtx_norm = np.array([
            [1/self.fx,0,0],
            [0,1/self.fy,0],
            [0,0,1]
        ])
        # opposite of mtx_norm
        self.mtx_norm_rev = np.array([
            [self.fx,0,0],
            [0,self.fy,0],
            [0,0,1]
        ])

    def _create_mtx_rot_mv(self):
        '''
        Create matrix to rotate & move image & to do opposite.
    
        Parameters
        ----------
        None
    
        Returns
        -------
        None
        '''
        # matrix to rotate coordinates around axis of camera coordinate system
        mtx_x_rotate = np.array([
            [1,0,0],
            [0,self.cos_x,-self.sin_x],
            [0,self.sin_x,self.cos_x]
        ])
        mtx_x_rotate_rev = np.array([
            [1,0,0],
            [0,self.cos_x_rev,-self.sin_x_rev],
            [0,self.sin_x_rev,self.cos_x_rev]
        ])

        mtx_y_rotate = np.array([
            [self.cos_y,0,self.sin_y],
            [0,1,0],
            [-self.sin_y,0,self.cos_y]
        ])
        mtx_y_rotate_rev = np.array([
            [self.cos_y_rev,0,self.sin_y_rev],
            [0,1,0],
            [-self.sin_y_rev,0,self.cos_y_rev]
        ])

        mtx_z_rotate = np.array([
            [self.cos_z,-self.sin_z,0],
            [self.sin_z,self.cos_z,0],
            [0,0,1]
        ])
        mtx_z_rotate_rev = np.array([
            [self.cos_z_rev,-self.sin_z_rev,0],
            [self.sin_z_rev,self.cos_z_rev,0],
            [0,0,1]
        ])
    
        # matrix to move origin
        mtx_x_mv = np.array([
            [1,0,0],
            [0,1,self.sin_x],
            [0,0,1]
        ])
        mtx_x_mv_rev = np.array([
            [1,0,0],
            [0,1,self.sin_x_rev],
            [0,0,1]
        ])

        mtx_y_mv = np.array([
            [1,0,-self.sin_y],
            [0,1,0],
            [0,0,1]
        ])
        mtx_y_mv_rev = np.array([
            [1,0,-self.sin_y_rev],
            [0,1,0],
            [0,0,1]
        ])
    
        # matrix to rotate & move
        # from low angle to looks like taken from directly above
        self.mtx_rot_mv = mtx_z_rotate @ (mtx_y_mv @ (mtx_y_rotate @ (mtx_x_mv @ mtx_x_rotate)))
#        self.mtx_rot_mv = mtx_y_mv @ (mtx_y_rotate @ (mtx_z_rotate @ (mtx_x_mv @ mtx_x_rotate)))
        # opposite of mtx_rot_mv
        self.mtx_rot_mv_rev = mtx_x_rotate_rev @ (mtx_x_mv_rev @ (mtx_y_rotate_rev @ (mtx_y_mv_rev @ mtx_z_rotate_rev)))
#        self.mtx_rot_mv_rev = mtx_x_rotate_rev @ (mtx_x_mv_rev @ (mtx_z_rotate_rev @ (mtx_y_rotate_rev @ mtx_y_mv_rev)))

    def _create_mtx_magnification_and_mtx_mv_origin_topleft(self):
        '''
        Create matrix to magnify normalized image & to do opposite. \n
        Create matrix to move origin to top-left of transformed image & to do opposite.
    
        Parameters
        ----------
        None
    
        Returns
        -------
        None
        '''
        # create matrix for calculation
        calc_coord = np.ones((4, 3)).astype(int)

        # x coordinates in column 0 & y coordinates in column 1
        calc_coord[:,:2] = np.array([
            [0, 0],
            [self.img_width - 1, 0],
            [self.img_width - 1, self.img_height - 1],
            [0, self.img_height - 1]
        ])

        # move origin from top-left to center
        calc_coord = calc_coord @ self.mtx_mv_origin_center.T
        
        # normalize coordinate points by focal length values
        calc_coord = calc_coord @ self.mtx_norm.T
    
        # rotate & move coordinate points
        calc_coord = calc_coord @ self.mtx_rot_mv.T
    
        # normalize coordinate points by column 2 values
        calc_coord[:,0] = calc_coord[:,0] / calc_coord[:,2]
        calc_coord[:,1] = calc_coord[:,1] / calc_coord[:,2]
    
        # calculate magnification ratio of width & height to original size, respectively, & adopt smaller one
        magnification_ratio_width = self.img_width / abs(np.max(calc_coord[:,0]) - np.min(calc_coord[:,0]))
        magnification_ratio_height = self.img_height / abs(np.max(calc_coord[:,1]) - np.min(calc_coord[:,1]))
        magnification_ratio = magnification_ratio_width if magnification_ratio_width < magnification_ratio_height else magnification_ratio_height
        # matrix to magnify normalized image
        self.mtx_magnification = np.array([
            [magnification_ratio,0,0],
            [0,magnification_ratio,0],
            [0,0,1]
        ])
        # opposite of mtx_magnification
        self.mtx_magnification_rev = np.array([
            [1/magnification_ratio,0,0],
            [0,1/magnification_ratio,0],
            [0,0,1]
        ])

        # magnify coordinate points
        calc_coord = calc_coord @ self.mtx_magnification.T

        mv_x = -(np.max(calc_coord[:,0]) + np.min(calc_coord[:,0])) / 2 + self.img_width / 2
        mv_y = -(np.max(calc_coord[:,1]) + np.min(calc_coord[:,1])) / 2 + self.img_height / 2
        # matrix to move origin to top-left of transformed image
        self.mtx_mv_origin_topleft = np.array([
            [1,0,mv_x],
            [0,1,mv_y],
            [0,0,1]
        ])
        # opposite of mtx_mv_origin_topleft
        self.mtx_mv_origin_topleft_rev = np.array([
            [1,0,-mv_x],
            [0,1,-mv_y],
            [0,0,1]
        ])

    def _create_mtx_trans(self):
        '''
        Create matrix to transform image from low angle to looks like taken from directly above & to do opposite.
    
        Parameters
        ----------
        None
    
        Returns
        -------
        None
        '''
        # create matrix to transform image
        # from low angle to looks like taken from directly above
        self.mtx_trans = self.mtx_mv_origin_topleft @ (self.mtx_magnification @ (self.mtx_rot_mv @ (self.mtx_norm @ self.mtx_mv_origin_center)))
        # from looks like taken from directly above to low angle
        self.mtx_trans_rev = self.mtx_mv_origin_center_rev @ (self.mtx_norm_rev @ (self.mtx_rot_mv_rev @ (self.mtx_magnification_rev @ self.mtx_mv_origin_topleft_rev)))

    def transform_depth_image(self, depth_img):
        '''
        Transform depth image taken from angle into it looks like taken from directly above.

        Parameters
        ----------
        depth_img : NumPy Array
            depth image
    
        Returns
        -------
        ret_depth_img : NumPy Array
            depth image
        '''
        # generate 2-dim matrix with vertical & horizontal pixel number of image
        pixcel_num = np.mgrid[0:self.img_height,0:self.img_width]

        # create matrix for normalize camera coordinates
        camera_coordinates = np.ones((self.img_width*self.img_height,3))

        # x coordinates in column 0
        # y coordinates in column 1
        camera_coordinates[:,0] = pixcel_num[1,:,:].flatten()
        camera_coordinates[:,1] = pixcel_num[0,:,:].flatten()

        # convert to camera coordinates
        camera_coordinates = camera_coordinates @ self.mtx_mv_origin_center.T

        # normalize camera coordinates
        camera_coordinates = camera_coordinates @ self.mtx_norm.T

        z_points = depth_img.flatten()
        # depth in column 2 as z-coordinates
        camera_coordinates[:,2] = z_points
        # scale normalized x and y coordinates to z coordinates (depth)
        camera_coordinates[:,0] *= z_points
        camera_coordinates[:,1] *= z_points

        # rotate & move camera coordinates
        camera_coordinates = camera_coordinates @ self.mtx_rot_mv.T

        # convert depth to 2-dim matrix
        depth_2dim = camera_coordinates[:,2].reshape(self.img_height, self.img_width)

        # homography transform depth image
        ret_depth_img = cv2.warpPerspective(depth_2dim, self.mtx_trans, (self.img_width, self.img_height))

        ret_depth_img = np.round(ret_depth_img).astype(int)

        return ret_depth_img

    def transform_bgr_image(self, bgr_img):
        '''
        Transform BGR image taken from angle into it looks like taken from directly above.

        Parameters
        ----------
        bgr_img : NumPy Array
            color image with BGR format
    
        Returns
        -------
        ret_bgr_img : NumPy Array
            color image with BGR format
        '''
        ret_bgr_img = cv2.warpPerspective(bgr_img, self.mtx_trans, (self.img_width, self.img_height))

        return ret_bgr_img

    def conv_coord_to_origin(self, coordinates):
        '''
        Convert coordinates on image looks like taken from directly above to those on image taken from low angle.

        Parameters
        ----------
        coordinates : NumPy Array
            coordinates as [[x0, y0],[x1, y1], ... ]
    
        Returns
        -------
        ret_coordinates : NumPy Array
            coordinates as [[x0, y0],[x1, y1], ... ]
        '''
        # get the number of coordinates
        coordinates_num = coordinates.shape[0]

        # create matrix for calculation
        calc_coord = np.ones((coordinates_num, 3)).astype(int)

        # x coordinates in column 0 & y coordinates in column 1
        calc_coord[:,:2] = coordinates
    
        # move origin from top-left to optical axis
        calc_coord = calc_coord @ self.mtx_mv_origin_topleft_rev.T
        
        # normalize coordinate points by focal length values
        calc_coord = calc_coord @ self.mtx_magnification_rev.T
    
        # transform from looks like taken from directly above to low angle
        calc_coord = calc_coord @ self.mtx_rot_mv_rev.T
    
        # normalize coordinate points by column 2 values
        calc_coord[:,0] = calc_coord[:,0]/calc_coord[:,2]
        calc_coord[:,1] = calc_coord[:,1]/calc_coord[:,2]
    
        # put rotated coordinate points on image plane (opposite of mtx_norm)
        calc_coord = calc_coord @ self.mtx_norm_rev.T
    
        # move origin from optical axis to top-left (opposite of mtx_mv_origin_center)
        calc_coord[:,0] = calc_coord[:,0] + self.ppx
        calc_coord[:,1] = calc_coord[:,1] + self.ppy
    
        ret_coordinates = calc_coord[:,:2].astype(int)

        return ret_coordinates

# debug start ----------------------------------------------------------------------
if __name__ == "__main__":
    dirctory = "./data/99_result"
    if os.path.isdir(dirctory):
        shutil.rmtree(dirctory)
    if not os.path.isdir(dirctory):
        os.mkdir(dirctory)
    
    bgr_img = cv2.imread("./data/00_src/low_angle/img0.png")
    depth_img = cv2.imread("./data/00_src/low_angle/depth0.png", -1)
    focal, pp = (924.277379745929, 925.7384642369517), (640, 360)
    angle = (22.963773059854553, 0, 0)

#    img_corner = np.array([[0,0],[img_size[1]-1,0],[img_size[1]-1,img_size[0]-1],[0,img_size[0]-1]])

#    cv2.drawContours(bgr_img, [img_corner], 0, (0,0,255), 3)
#    cv2.circle(bgr_img, (int(img_size[1]/2), int(img_size[0]/2)), 2, (0,0,255), thickness=-1)

    src_depth_img_255 = depth_img/np.max(depth_img)*255
    cv2.imwrite("./data/99_result/depth0_255.png", src_depth_img_255.astype(int))    
    
    start_time = time.time()
    trans = homographyTransform(focal, pp, angle)
    transformed_bgr_img = trans.transform_bgr_image(bgr_img)
    transformed_depth_img = trans.transform_depth_image(depth_img)
    end_time = time.time()
    print("homography_transform: " + str(int(np.ceil((end_time - start_time)*1000))) + " [ms]")

#    cv2.circle(transformed_bgr_img, (int(transformed_bgr_img.shape[1]/2), int(transformed_bgr_img.shape[0]/2)), 2, (255,255,0), thickness=-1)
    cv2.imwrite("./data/99_result/transformed_img0.png", transformed_bgr_img)
    transformed_depth_img_255 = transformed_depth_img/np.max(transformed_depth_img)*255
    cv2.imwrite("./data/99_result/transformed_depth0.png", transformed_depth_img_255.astype(int))

# debug end ----------------------------------------------------------------------