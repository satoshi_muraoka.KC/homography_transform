#!/bin/bash

USER_HOME=${HOME}
USER_ID=$(id -u)
USER_NAME=${USER}
GROUP_ID=$(id -g)
GROUP_NAME=$(getent group ${GROUP_ID} | cut -d: -f1)

FTP_PROXY=${ftp_proxy}
HTTP_PROXY=${http_proxy}
HTTPS_PROXY=${https_proxy}

# work directory path @ host
HOST_MNT_DIR=`pwd | sed -e 's#/docker$##g'`
# mount directory @ container
CONTAINER_MNT_DIR=${HOST_MNT_DIR}
# work directory name (to make docker image name)
WORK_DIR_NAME=`echo "${HOST_MNT_DIR}" | sed -e 's/.*\/\([^\/]*\)$/\1/'`

IMAGE_NAME="${USER_NAME}/${WORK_DIR_NAME}"
CONTAINER_NAME="${USER_NAME}_${WORK_DIR_NAME}"
TAG="latest"

BUILD_OPTION="\
 --force-rm=true\
 --build-arg http_proxy=${http_proxy} \
 --build-arg https_proxy=${http_proxy} \
 --build-arg ftp_proxy=${http_proxy} \
 --build-arg USER_ID=${USER_ID}\
 --build-arg USER_NAME=${USER_NAME}\
 --build-arg CONTAINER_MNT_DIR=${CONTAINER_MNT_DIR}\
 -f ${DOCKER_DIR}/Dockerfile\
 -t ${IMAGE_NAME}:${TAG}\
 .\
"

RUN_OPTION="\
 -v ${CONTAINER_MNT_DIR}:${HOST_MNT_DIR}\
 -e http_proxy=${HTTP_PROXY}\
 -e https_proxy=${HTTPS_PROXY}\
 -e ftp_proxy=${FTP_PROXY}\
 --name ${CONTAINER_NAME}\
 -it\
 --rm ${IMAGE_NAME}\
"