#!/bin/bash
DOCKER_DIR=`dirname ${0}`
source ${DOCKER_DIR}/settings.sh ${DOCKER_DIR}

echo "docker build ${BUILD_OPTION}"
docker build ${BUILD_OPTION}
