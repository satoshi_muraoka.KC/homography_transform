#!/bin/bash
DOCKER_DIR=`dirname ${0}`
source ${DOCKER_DIR}/settings.sh ${DOCKER_DIR}

echo "docker run ${RUN_OPTION}"
docker run ${RUN_OPTION}
