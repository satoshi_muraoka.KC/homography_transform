# Homography_transform

Program to convert an image taken from an angle into a homographic image as if it were taken from directly above.

# DEMO

### before:

<img src="./data/demo/img0.png" width="350">

### after:

<img src="./data/demo/transformed_img0.png" width="350">

# Usage

1. Clone the git repository locally.

```
$ git clone ${cloneURL}
```

2. Run the shell script in the homography_transform directory to build the docker image.

```
$ ./docker/build_image.sh
```

3. Run the shell script in the homography_transform directory to run and exec container.

```
$ ./docker/run_exec_container.sh
```

4. Run the python script in the container.

```
$ python homography_transform.py
```

5. The converted image will be output to data/99_result directory.

# Note

The data/99_result directory is deleted and recreated each time the python script is run. Therefore, if there is any important data left in this directory, it should be saved.

<!--
# Name（リポジトリ/プロジェクト/OSS などの名前）

分かりやすくてカッコイイ名前をつける（今回は"hoge"という名前をつける）

"hoge"が何かを簡潔に紹介する

# DEMO

"hoge"の魅力が直感的に伝えわるデモ動画や図解を載せる

# Features

"hoge"のセールスポイントや差別化などを説明する

# Requirement

"hoge"を動かすのに必要なライブラリなどを列挙する

- huga 3.5.2
- hogehuga 1.0.2

# Installation

Requirement で列挙したライブラリなどのインストール方法を説明する

```bash
pip install huga_package
```

# Usage

DEMO の実行方法など、"hoge"の基本的な使い方を説明する

```bash
git clone https://github.com/hoge/~
cd examples
python demo.py
```

# Note

注意点などがあれば書く

# Author

作成情報を列挙する

- 作成者
- 所属
- E-mail

# License

ライセンスを明示する

"hoge" is under [MIT license](https://en.wikipedia.org/wiki/MIT_License).

社内向けなら社外秘であることを明示してる

"hoge" is Confidential.
-->
